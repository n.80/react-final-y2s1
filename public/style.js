// // When the user scrolls the page, execute myFunction
// window.onscroll = function() {myFunction()};

// // Get the navbar
// var navbar = document.getElementById("navbar");

// // Get the offset position of the navbar
// var sticky = navbar.offsetTop;

// // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
// function myFunction() {
//   if (window.pageYOffset >= sticky) {
//     navbar.classList.add("sticky")
//   } else {
//     navbar.classList.remove("sticky");
//   }
// }

// function hideNavBar() {
//   var x = document.getElementById("hidden-nav");
//   var y = document.getElementById("body");
//   if (x.style.width == "100%") {
//     x.style.width = "0";
//     x.style.left = "100%";
//   } 
//   else {
//     x.style.width = "100%";
//     x.style.left = "0";
//   }

//   if (y.style.position == "fixed") {
//     y.style.position = "static";
//   }
//   else {
//     y.style.position = "fixed";
//   }
// }

var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("slide");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";
}
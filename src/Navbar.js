import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Link } from "react-router-dom";

import "./css/Main.css";
import { dom } from '@fortawesome/fontawesome-svg-core';

const Navbar = () => (
    <div className="navbar navi">
        <div className="line">
            <Link to="/">
                <h2>Travelo</h2>
            </Link>
        </div>

        <div className="line">
            <ul className="menu">
                <Link to="/destinations">
                    <li>Destination</li>
                </Link>
                <Link to="/discount">
                    <li>Discount</li>
                </Link>
                <Link to="/about">
                    <li>About</li>
                </Link>
                <Link to="/blog">
                    <li>Blog</li>
                </Link>
                <Link to="/contact">
                    <li>Contact</li>
                </Link>
            </ul>
        </div>

        <div className="line">
            <ul className="menu">
                <Link to="/login">
                    <li>Login</li>
                </Link>
                <Link to="signup">
                    <li>Sign Up</li>
                </Link>
            </ul>
        </div>

    </div>
)

export default Navbar
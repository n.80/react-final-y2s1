import React, { Component } from 'react'
import { Row, Col, Container} from 'react-bootstrap'

import { Booking, BlogCom, HeaderCom } from "./Modules"
import Footer from "./Footer"

import Snow from './img/snow.jpg'
import Sweden from './img/sweden.jpg'
import Traveler from './img/traveler.jpg'
import Norway from './img/norway_tour.jpg'
import Couple from './img/couple_trip1.jpg'
import Family from './img/FamilyTravelAirportNicoElNinoAdobe.jpg'
import Solo from "./img/solo.jpg"

import "./css/Main.css"


const Header = () => (
    <div>
        <HeaderCom
            src={Solo}
            title="Our Blog"
        />
    </div>
)

export class Blogs extends Component {
    render() {
        return (
            <Container>
                <div className="section">
                    <h1 className="primary-color text-center">Our Blog</h1>
                    <p className="text-center mb-4">See Our Daily News & Updates</p>
                </div>
                <Row>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Family}
                            alt="Family"
                            blogTitle="How to Plan Your Family Trip"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Traveler}
                            alt="Girl with Effiel Tower"
                            blogTitle="Need a Solo Trip?"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                </Row>

                <Row>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Sweden}
                            alt="Aurora"
                            blogTitle="Where and When to Watch the Most Beautiful Aurora"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Norway}
                            alt="Couple with aurora"
                            blogTitle="How to Plan Your Location"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                </Row>

                <Row>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Couple}
                            alt="Couple trip"
                            blogTitle="Plan your Unforgettable Honeymoon Trip"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                    <Col lg={6} md={12} className="blog">
                        <BlogCom 
                            src={Snow}
                            alt="snow"
                            blogTitle="Best Places to Visit in Winter"
                            blogDate="by Theresa Winston • Jan 18, 2019 at 2:00 pm • News"
                            blogText="Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                        />
                    </Col>
                </Row>
            </Container>
        )
    }
}


export class Blog extends Component {
    render() {
        return (

            <div className="blog-page">
                <Header />
                <Blogs></Blogs>
                <Booking/>
                <Footer></Footer>
            </div>
        )
    }
}

export default Blog

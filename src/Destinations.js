import React from 'react';
import ReactDOM from 'react-dom';

import { DestinationsCom, ServicesCom, TestimonialsCom, Title, HeaderCom } from './Modules'
import Footer from './Footer'

import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import "./css/Main.css"

import romantic from './img/romantic.jpg'
import fj from './img/japan1.jpg'
import tl from './img/thailand1.jpg'
import gr from './img/greece1.jpg'
import el from './img/england1.jpg'
import ef from './img/france1.jpg'
import pr from './img/peru1.jpg'
import ct from './img/city.jpg';
import mt from './img/mountain.jpg';
import nt from './img/natural.jpg';

const Header = () => (
    <div>
        <HeaderCom
            src={romantic}
            title="Destinations"
        />
    </div>
)

const Price = () => (
    <div class="price primary-color">
        <p>$599</p>
    </div>
)

const Destiantion = () => (
    <Container className="section">
        <Title title="Destinations"/>
        <Row>
            <Col lg={4} md={6}>
                <DestinationsCom src={fj} title='Japan'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={gr} title='Greece'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={tl} title='Thailand'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={el} title='England'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={ef} title='France'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={pr} title='Peru'/>
                <Price/>
            </Col>
        </Row>
    </Container>
)

class Testimonials extends React.Component {
    
    componentDidMount () {
        const script = document.createElement("script");
    
        script.src = "./style.js";
        script.async = true;
    
        document.body.appendChild(script);
    }

    render() {
        return(
            <Container className="section">
                <Title title="Testimonials"/>
                <div className="slide-container">
                    <TestimonialsCom
                        src={ct}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={mt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={nt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <div class="slide-button">
                        <a><i class="fas fa-chevron-left button-left"></i></a>
                        <a><i class="fas fa-chevron-right button-right"></i></a>
                    </div>
                </div>
            </Container>
        )
    }
}

const Services = () => (
    <Container className="section">
        <Row>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-plane"
                    title="Air Ticket" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-ship"
                    title="Cruises" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-search-location"
                    title="Tour Packages" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
        </Row>
    </Container>
)

class Destinations extends React.Component {
    render() {
        return(
            <div className="destinations-page">
                <Header/>
                <Destiantion/>
                <Testimonials/>
                <Services/>
                <Footer/>
            </div>
        )
    }
}

export default Destinations
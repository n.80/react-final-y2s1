import React from 'react';
import ReactDOM from 'react-dom';

import { DestinationsCom, ServicesCom, TestimonialsCom, Title, HeaderCom } from './Modules'
import Footer from './Footer'

import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import "./css/Main.css"

import ancient from './img/ancient.jpg'
import sh from './img/stonehenge.jpg'
import pr from './img/peru1.jpg'
import tk from './img/turkey1.jpg'
import et from './img/easter.jpg'
import gz from './img/5616.jpg'
import my from './img/mayan.jpg'
import ct from './img/city.jpg';
import mt from './img/mountain.jpg';
import nt from './img/natural.jpg';

const Header = () => (
    <div>
        <HeaderCom
            src={ancient}
            title="Discount"
        />
    </div>
)

const Price = () => (
    <div class="price primary-color">
        <p><span class="cross-text">$599</span> $399</p>
    </div>
)


const Destiantion = () => (
    <Container className="section">
        <Title title="We offer some Discounts"/>
        <Row>
            <Col lg={4} md={6}>
                <DestinationsCom src={sh} title='United Kindom'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={pr} title='Peru'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={tk} title='Turkey'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={et} title='Chile'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={gz} title='Egypt'/>
                <Price/>
            </Col>
            <Col lg={4} md={6}>
                <DestinationsCom src={my} title='Mexico'/>
                <Price/>
            </Col>
        </Row>
    </Container>
)

class Testimonials extends React.Component {
    
    componentDidMount () {
        const script = document.createElement("script");
    
        script.src = "./style.js";
        script.async = true;
    
        document.body.appendChild(script);
    }

    render() {
        return(
            <Container className="section">
                <Title title="Testimonials"/>
                <div className="slide-container">
                    <TestimonialsCom
                        src={ct}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={mt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={nt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <div class="slide-button">
                        <a><i class="fas fa-chevron-left button-left"></i></a>
                        <a><i class="fas fa-chevron-right button-right"></i></a>
                    </div>
                </div>
            </Container>
        )
    }
}

const Services = () => (
    <Container className="section">
        <Title title="Services"/>
        <Row>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-plane"
                    title="Air Ticket" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-ship"
                    title="Cruises" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-search-location"
                    title="Tour Packages" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-hotel"
                    title="Hotel" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-water"
                    title="Sea Explorations" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-skiing"
                    title="Ski Experiences" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
        </Row>
    </Container>
)

class Discount extends React.Component {
    render() {
        return(
            <div className="destinations-page">
                <Header/>
                <Destiantion/>
                <Testimonials/>
                <Services/>
                <Footer/>
            </div>
        )
    }
}

export default Discount
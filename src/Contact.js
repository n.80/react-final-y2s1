import React, { Component } from 'react'

import Footer from "./Footer"
import { HeaderCom } from "./Modules"

import { Button, Row, Col, Container, Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import './css/Main.css'

import Mountain2 from "./img/mountain2.jpg"
import contact from "./img/contact.jpg"

const Header = () => (
    <div>
        <HeaderCom
            src={contact}
            title="Contact Us"
        />
    </div>
)

const CoForm = () => (
    <Container className="form-container pt-4 mt-4">
        <Row>
            <Col lg={8} sm={12} className="contact-form secondary-background px-5 py-5">
                <Form>
                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group>
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type="text"/>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={6}>
                            <Form.Group>
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="text"/>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group>
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email"/>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <Form.Group>
                                <Form.Label>Message</Form.Label>
                                <Form.Control as="textarea" rows={10}/>
                            </Form.Group>
                        </Col>
                        <Col md={12}>
                            <a><div type="submit" className="btnStyle mx-0">Send Message</div></a>
                        </Col>
                    </Row>
                    
                </Form>
            </Col>

        
            <Col className="address" lg={4} sm={12}>
                <div id="address">
                    <h2>Address</h2>
                    <p>St. 235 No. #65 District, Commune, Phnom Penh</p>
                    <h2>Phone</h2>
                    <p>+012 345 678</p>
                    <h2>Email Address</h2>
                    <p>travelo@email.com</p>
                </div>
                
                <div id="more-info">
                    <h2 className="mt-3">More info</h2>
                    <img src={Mountain2} style={{width: "100%"}} className="mb-2"></img>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Ipsa ad iure porro mollitia architecto hic consequuntur. 
                        Distinctio nisi perferendis dolore, ipsa consectetur? Fugiat quaerat eos qui,
                        libero neque sed nulla.
                    </p>
                    <a href="#"><div type="text" className="btnStyle mx-0">Learn More</div></a>
                </div>
            </Col>
        </Row>        
    </Container>
)

export class Contact extends Component {
    render() {
        return (
            <div className="contact-page">
                <Header/>
                <CoForm />
                <Footer />
            </div>
        )
    }
}

export default Contact

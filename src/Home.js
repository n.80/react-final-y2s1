import React from 'react';
import ReactDOM from 'react-dom';

import { DestinationsCom, ServicesCom, TestimonialsCom, Title, HeaderCom } from './Modules'
import Footer from './Footer'

import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import "./css/Main.css"

import gw from './img/greatwall_china.jpg';
import kt from './img/kyoto_japan.jpg';
import tj from './img/tajmahal_india.jpg';
import rm from './img/rome_italy.jpg';
import pr from './img/pyramid_egypt.jpg';
import lb from './img/liberty_newyork.jpg';
import ct from './img/city.jpg';
import mt from './img/mountain.jpg';
import nt from './img/natural.jpg';
import tr from './img/travel.jpg';


const Header = () => (
    <div>
        <HeaderCom
            src={tr}
            title="Explore New Places"
        />

        <div className="flex-container">
            <div>
                <DestinationsCom src={ct} title='City'/>
            </div>
            <div>
                <DestinationsCom src={mt} title='Mountain'/>
            </div>
            <div>
                <DestinationsCom src={nt} title='Natural'/>
            </div>               
        </div>
    </div>
)

const HeaderServices = () => (
    <Container className="section">
        <Row>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-plane"
                    title="Air Ticket" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-ship"
                    title="Cruises" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={6} xs={12}>
                <ServicesCom 
                    icon="fas fa-search-location"
                    title="Tour Packages" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
        </Row>
    </Container>
)

class Testimonials extends React.Component {
    
    componentDidMount () {
        const script = document.createElement("script");
    
        script.src = "./style.js";
        script.async = true;
    
        document.body.appendChild(script);
    }

    render() {
        return(
            <Container className="section">
                <Title title="Testimonials"/>
                <div className="slide-container">
                    <TestimonialsCom
                        src={ct}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={mt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <TestimonialsCom
                        src={nt}
                        description="
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!, et non dignissimos culpa?
                        Ut veritatis, quos illum totam quis blanditiis, minima minus odio!"
                    />
                    <div class="slide-button">
                        <a><i class="fas fa-chevron-left button-left"></i></a>
                        <a><i class="fas fa-chevron-right button-right"></i></a>
                    </div>
                </div>
            </Container>
        )
    }
}

const Destiantions = () => (
    <Container className="section">
        <Title title="Destinations"/>
        <Row>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={gw} title='China'/>
            </Col>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={kt} title='Japan'/>
            </Col>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={tj} title='India'/>
            </Col>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={rm} title='Italy'/>
            </Col>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={pr} title='Egypt'/>
            </Col>
            <Col lg={4} md={6} className="destination">
                <DestinationsCom src={lb} title='America'/>
            </Col>
        </Row>
    </Container>
)

const Services = () => (
    <Container className="section">
        <Title title="Services"/>
        <Row>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-plane"
                    title="Air Ticket" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-ship"
                    title="Cruises" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-search-location"
                    title="Tour Packages" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-hotel"
                    title="Hotel" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-water"
                    title="Sea Explorations" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-skiing"
                    title="Ski Experiences" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
        </Row>
    </Container>
)


class Home extends React.Component {
    render() {
        return(
            <div className="home-page">
                <Header/>
                <HeaderServices/>
                <Testimonials/>
                <Destiantions/>
                <Services/>
                <Footer/>
            </div>
        )
    }
}

export default Home;
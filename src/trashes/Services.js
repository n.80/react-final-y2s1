import React from 'react';
import "./Services.css"

export const Services = (props) => {
    <div>
        <div>
            # Icons
        </div>

        <div>
            <h4>{props.title}</h4>
            <p>{props.description}</p>
            <a href="#" className="learn-more">Learn more</a>
        </div>
    </div>
}
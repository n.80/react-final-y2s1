import React from 'react';
import "./Destination.css"

const DestinationsCom = (props) => (
    <div className="destination">
        <a href="#" className="image-wrapper">
            <img src={props.src} class="zoom"></img>

            <div className="image-text">
                <h3>{props.title}</h3>
            </div>
        </a>
    </div>
)

export default DestinationsCom
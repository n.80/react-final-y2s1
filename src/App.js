import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Login from './Login.js'
import Home from './Home'
import About from './About';
import Contact from './Contact.js';
import Destinations from './Destinations.js';
import Discount from './Discount.js';
import Navbar from "./Navbar"
import Blog from './Blog.js';
import SignUp from './SignUp.js';

import "./css/Main.css"


const App = () => (
    <Router>
        <div>
            <Navbar/>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/destinations" component={Destinations}/>
                <Route path="/discount" component={Discount}/>
                <Route path="/about" component={About}/>
                <Route path="/contact" component={Contact}/>
                <Route path="/blog" component={Blog}/>
                <Route path="/login" component={Login}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </div>
    </Router>
)

export default App